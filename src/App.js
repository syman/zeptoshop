import './App.css';
import { BrowserRouter, Route, Link,Routes, Navigate } from "react-router-dom";
import Header from './components/inc/Header';
import Home from './components/pages/Home';
import ProductDetails from './components/pages/ProductDetails';
import CreateProduct from './components/pages/CreateProduct';
import Login from "./components/pages/Login";
import Registration from "./components/pages/Registration";
import {getToken, getAdmin} from "./components/inc/Auth";
import Productslist from "./components/pages/Productslist";
import UpdateProduct from "./components/pages/UpdateProduct";

function App() {
  return (
    <>
    <BrowserRouter>
      <Header />
      <Routes>
        <Route exaxt path='/' element={<Home />} />
        <Route path='/login' element={ !getToken() ? <Login /> : <Navigate to={{ pathname: '/' }} />} />
        <Route path='/registration'  element={ !getToken() ? <Registration /> : <Navigate to={{ pathname: '/' }} />} />
        <Route path='/products/:id' element={ <ProductDetails/>}/>
        <Route path='/products/edit/:id' element={ <UpdateProduct/>}/>
        <Route path='/product/create' element={ getAdmin() ? <CreateProduct /> : <Navigate to={{ pathname: '/login' }} />} />
        <Route path='/product/list' element={ getAdmin() ? <Productslist /> : <Navigate to={{ pathname: '/login' }} />} />
      </Routes>
    </BrowserRouter>
    </>
  );
}

export default App;

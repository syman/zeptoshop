import {ActionTypes} from '../contants/action-types';
import axios from "axios";
export const setProduct = (products) =>{
    return {
        type: ActionTypes.SET_PRODUCTS,
        payload: products
    }
}

const loadProducts = (searchItem = null)=>{
    return function (dispach){
        axios.get('http://localhost/zeptoShopApi/public/api',{ params: { search: searchItem } })
            .then((response)=>{
                dispach(setProduct(response));
            }).catch((err) => {
            console.log(err);

        });
    }
}

export const selecttProduct = (products) =>{
    return {
        type: ActionTypes.SELECTED_PRODUCT,
        payload: products
    }
}

export const removeSelectedProduct = (products) =>{
    return {
        type: ActionTypes.REMOVE_SELECTED_PRODUCT,
        payload: products
    }
}

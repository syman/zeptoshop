import React from 'react'
import {Navbar, Nav, Container, NavDropdown} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap'
import {getAdmin, getToken, getUser, removeUserSession} from "./Auth";
import axios from "axios";
import {useNavigate} from "react-router-dom";

const Header = () => {
    const user = getUser();
    const token = getToken();
    const navigate = useNavigate();
    const handleLogout = () => {
        axios.post('http://localhost/zeptoShopApi/public/api/logout','', {
            headers:{ 'Authorization': `Bearer ${token}`}
        })
        removeUserSession();
        navigate('/');

    }
    return (
        <Navbar className='mb-5' bg="light" expand="lg">
          <Container>
            <Navbar.Brand href="#home">Zepto- Shop</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                  <Nav.Link href="/">Home</Nav.Link>
                  {getAdmin() ? (
                    <Nav.Link href="/product/create">Create product</Nav.Link>
                  ):''}
                  {getAdmin() ? (
                    <Nav.Link href="/product/list">Product list</Nav.Link>
                  ):''}
              </Nav>
            <nav className='justify-content-end'>
                {user ? (
                    <NavDropdown title={user.name} id="basic-nav-dropdown-logout">
                        <NavDropdown.Item onClick={handleLogout}>Logout</NavDropdown.Item>
                    </NavDropdown>
                ):(
                    <NavDropdown  title="Account" id="basic-nav-dropdown">
                        <NavDropdown.Item href="/login">Login</NavDropdown.Item>
                        <NavDropdown.Item href="/registration">Registration</NavDropdown.Item>
                    </NavDropdown>
                )}


            </nav>
            </Navbar.Collapse>
          </Container>
      </Navbar>
    )
}

export default Header

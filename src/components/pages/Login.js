import axios from 'axios';
import React, { useEffect,useState } from 'react';
import {Form, Button, Card, Alert} from "react-bootstrap";
import CardHeader from "react-bootstrap/CardHeader";
import {useNavigate} from "react-router-dom";
import {setUserSession} from "../inc/Auth";


const Login = () => {
    const [user , setUser] = useState({
        email: '',
        password: ''
    });
    const [error, setError] = useState(null);
    const inpurHandaler = (e) => {
        var name = e.target.name;
        var value = e.target.value;
        setUser({...user,[name]:value});
    }
    const navigate = useNavigate();
    const formHandaler = (e) =>{
        e.preventDefault();
        axios.post('http://localhost/zeptoShopApi/public/api/login',user).then((response) =>{
                setUserSession(response.data.data.token,response.data.data.user);
            navigate('/');
            }).catch((err) =>{
            setError(err.response.data.message);
                if (err.response.status === 401 || err.response.status === 400 || err.response.status === 422){
                    setError(err.response.data.message);
                }
            })
    }
    return (
        <Card className='h-100 m-auto' style={{ width: '25rem' }}>
            {error ? (
                <Alert variant="danger" onClose={() => setError(null)} dismissible>
                    <p>Oh snap! {error}</p>
                </Alert>
            ): ('')}
            <Card.Body>
                <Form onSubmit={formHandaler}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control onChange={inpurHandaler} type="email" name="email" placeholder="Enter email" />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control onChange={inpurHandaler} type="password" name="password" placeholder="Password" />
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Login
                    </Button>
                </Form>
            </Card.Body>
        </Card>
    )
}

export default Login

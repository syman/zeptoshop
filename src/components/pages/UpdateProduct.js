import React, {useEffect, useState} from 'react'
import {useNavigate, useParams} from "react-router-dom";
import {Button, Card, Form} from "react-bootstrap";
import {getToken} from "../inc/Auth";
import axios from "axios";

const UpdateProduct = () => {
  const param = useParams();
  const navigate = useNavigate();
  const token = getToken();
  const [productData, setProductData] = useState({
    title: '',
    price: ''
  });
  const inpurHandaler = (e) => {
    var name = e.target.name;
    var value = e.target.value;
    setProductData({...productData,[name]:value});
  }
  var formData = new FormData();
  const fileHandle = (e) => {
    const files = e.target.files;
    formData.append('image',files[0]);
  }

  const formHandaler = (e)=>{
    e.preventDefault();
    formData.append('title',productData.title);
    formData.append('price',productData.price);
    
    // console.log(formData.values());
    axios.post(`http://localhost/zeptoShopApi/public/api/products/${param.id}`,formData,{
      headers:{
        'Authorization': `Bearer ${token}`
      }
    }).then(res => {
      console.log(res.data.data);
      if (res.status === 200){
        setProductData({...productData,
          title: '',
          price: ''
        });
        navigate('/product/list');
        //swal('success','Blog created successfully','success')
      }
    }).catch((error)=>{
      console.log(error);
    });
  }
  const getProduct = async () =>{
    await axios.get(`http://localhost/zeptoShopApi/public/api/products/${param.id}`,{
      headers:{ 'Authorization': `Bearer ${token}`}
    }).then((res)=>{
      setProductData(res.data.data);
    })
  }

  useEffect(()=>{
    getProduct();
    // console.log(productData);
  },[]);

  return (
      <Card border="secondary" style={{ width: '60rem', margin: 'auto'}}>
        <Card.Header>Create product</Card.Header>
        <Card.Body>
          <Form onSubmit={formHandaler} encType="multipart/form-data">
            <Form.Group className="mb-3" >
              <Form.Label>Title</Form.Label>
              <Form.Control onChange={inpurHandaler} value={productData.title} id='title' name='title' type="text" placeholder="Product title" />
            </Form.Group>
            <Form.Group className="mb-3" >
              <Form.Label>Price</Form.Label>
              <Form.Control onChange={inpurHandaler} value={productData.price} id='price' name='price' type="number" placeholder="100" />
            </Form.Group>
            <Form.Group className="mb-3" >
              <Form.Label>Image</Form.Label>
              <Form.Control onChange={fileHandle} name='image' id='image' type="file" />
              <img src={productData.image} style={{width: "300px", height: 'auto'}} alt='' />
            </Form.Group>

            <Button variant="primary" type="submit">
              Update
            </Button>
          </Form>
        </Card.Body>
      </Card>
  )
}

export default UpdateProduct

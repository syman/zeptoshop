import React, {useEffect, useState} from 'react';
import {Table, Card, Alert} from "react-bootstrap";
import {useSelector} from "react-redux";
import axios from "axios";
import {setProduct} from "../../redux/actions/productActions";
import {Link} from "react-router-dom";
import {getToken} from "../inc/Auth";
import CardHeader from "react-bootstrap/CardHeader";

function Productslist(props) {
    const [product, setProduct] = useState();
    const token = getToken();
    const [successAlert, setSuccessAlert] = useState(false);
    const getProducts = async () => {
        const response = await axios.get('http://localhost/zeptoShopApi/public/api').then((res) =>{
         setProduct(res.data.data);
        }
        ).catch((err) =>{

        })
    }
    const renderedProducts = product?.map((pro,i) => {
        const {id, title, price} = pro;
        return(
            <tr key={i}>
                <td>{i+1}</td>
                <td><Link to={`/products/${id}`}>{title}</Link></td>
                <td>{price}</td>
                <td>
                    <Link className='m-3' to={`/products/edit/${id}`}>Edit</Link>
                    <button className='m-1' onClick={()=>{
                        axios.delete(`http://localhost/zeptoShopApi/public/api/products/${id}`,{
                            headers:{ 'Authorization': `Bearer ${token}`}
                        }).then(res => {
                            setSuccessAlert(true);
                            getProducts();
                        });
                    }} className="btn btn-danger" >Delete</button>

                </td>
            </tr>
        )
    })
    useEffect(()=>{
        getProducts();
    },[]);


    return (
        <>
            <Card border="secondary" style={{ width: '60rem', margin: 'auto'}}>
                <CardHeader>
                    {successAlert ? (
                        <Alert variant="success" onClose={() => setSuccessAlert(false)} dismissible>
                            <p>Product deleted successfully!</p>
                        </Alert>
                    ): ('')}
                </CardHeader>
                <Table striped bordered hover size="sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Product title</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    {renderedProducts ? renderedProducts : (
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    )}
                </tbody>
            </Table>
            </Card>
        </>
    );
}

export default Productslist;

import React , {useState} from 'react';
import axios from 'axios';
import { Form,Button, Card } from 'react-bootstrap';
import {getToken} from "../inc/Auth";
import {useNavigate} from "react-router-dom";

const CreateProduct = () => {
    const [productData, setProductData] = useState({
        title: '',
        price: ''
    });
    const navigate = useNavigate();
    var formData = new FormData();
    const token = getToken();
    const inpurHandaler = (e) => {
        var name = e.target.name;
        var value = e.target.value;
        setProductData({...productData,[name]:value});
    }
    const fileHandle = (e) => {
        const files = e.target.files;
        formData.append('image',files[0]);
        // createImage(files[0])
    }

    const formHandaler = (e)=>{
        e.preventDefault();
        formData.append('title',productData.title);
        formData.append('price',productData.price);
        console.log(formData);
        axios.post('http://localhost/zeptoShopApi/public/api/products',formData,{
            headers:{ 'Authorization': `Bearer ${token}`}
        }).then(res => {
            if (res.status === 201){
                setProductData({...productData,
                    title: '',
                    price: ''
                });
                e.target.reset();
                navigate('/');
                //swal('success','Blog created successfully','success')
            }
        });
    }
  return (
        <Card border="secondary" style={{ width: '60rem', margin: 'auto'}}>
            <Card.Header>Create product</Card.Header>
            <Card.Body>
                <Form onSubmit={formHandaler} encType="multipart/form-data">
                    <Form.Group className="mb-3" >
                        <Form.Label>Title</Form.Label>
                        <Form.Control onChange={inpurHandaler} id='title' name='title' type="text" placeholder="Product title" />
                    </Form.Group>
                    <Form.Group className="mb-3" >
                    <Form.Label>Price</Form.Label>
                        <Form.Control onChange={inpurHandaler} id='price' name='price' type="number" placeholder="100" />
                    </Form.Group>
                    <Form.Group className="mb-3" >
                    <Form.Label>Image</Form.Label>
                        <Form.Control onChange={fileHandle} name='image' id='image' type="file" />
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>
            </Card.Body>
        </Card>
  )
}

export default CreateProduct

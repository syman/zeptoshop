import axios from 'axios';
import React, { useEffect,useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import {Card, Button,Spinner} from 'react-bootstrap';
import { useParams } from 'react-router-dom'
import { selecttProduct } from '../../redux/actions/productActions';
import {getToken} from "../inc/Auth";

const ProductDetails = () => {
    const [waiting, setWaiting] = useState(true);
    const singleproduct = useSelector((state) => state.product);
    const {title,price,image} = singleproduct;
    const peram = useParams();
    const dispach = useDispatch();
    const getProduct = async () =>{
        const respnce = await axios.get(`http://localhost/zeptoShopApi/public/api/products/${peram.id}`).catch((err) =>{
            console.log(err);
        });
        dispach(selecttProduct(respnce.data.data));
        setWaiting(false);
    }
    useEffect(()=>{
        getProduct();
    },[peram.id]);
  return (
      <>
      {waiting ? (
      <Button variant="primary" disabled>
                <Spinner
                as="span"
                animation="grow"
                size="sm"
                role="status"
                aria-hidden="true"
                />
                Loading...
        </Button>) : (
            <Card>
            <Card.Img style={{ width: '18rem' }} variant="left" src={image} />
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>
                Price: {price}
                </Card.Text>
            </Card.Body>
            <Card.Footer>
            <Button variant="primary">Add to cart</Button>
            </Card.Footer>
        </Card>
        )}
      </>

  )
}

export default ProductDetails

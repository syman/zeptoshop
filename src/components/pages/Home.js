import axios from 'axios';
import React, { useEffect, useState } from 'react'
import {Row, Button, Container, Spinner,Form,Col} from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { setProduct } from '../../redux/actions/productActions';
import Product from './Product';
const Home = () => {
    const [searchItem, setSearchItem] = useState('');
    const dispach = useDispatch();
    // console.log(products);
    const [waiting, setWaiting] = useState(true);
    const getProducts = async () => {

        const response = await axios.get('http://localhost/zeptoShopApi/public/api',{ params: { search: searchItem } }).catch((err) =>{
            console.log(err);
        });
        dispach(setProduct(response.data.data));
        setWaiting(false);
    }
    const handleSearch = (e) =>{
        setSearchItem(e.target.value);
        getProducts();
    }
    useEffect(()=>{
        getProducts();
    },[]);
  return (
    <Container>
            <Row className='mb-5'>
                <Col>
                    <Form.Control onChange={handleSearch} placeholder="Search" />
                </Col>
            </Row>
        {waiting ? (<Button variant="primary" disabled>
                <Spinner
                as="span"
                animation="grow"
                size="sm"
                role="status"
                aria-hidden="true"
                />
                Loading...
        </Button>) : (<Row xs={1} md={4} className="g-4">
            <Product />
        </Row>)}



    </Container>

  )
}

export default Home;

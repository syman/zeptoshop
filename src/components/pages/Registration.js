import React, {useState} from 'react';
import {Alert, Button, Card, Form} from "react-bootstrap";
import axios from "axios";
import {useNavigate} from "react-router-dom";
import {setUserSession} from "../inc/Auth";

function Registration(props) {
    const [user , setUser] = useState({
        name:'',
        email: '',
        password: '',
        password_confirmation: ''
    });
    const [error, setError] = useState(null);
    const navigate = useNavigate();
    const inpurHandaler = (e) => {
        var name = e.target.name;
        var value = e.target.value;
        setUser({...user,[name]:value});
    }

    const formHandaler = (e) =>{
        e.preventDefault();
        axios.post('http://localhost/zeptoShopApi/public/api/registration',user).then((response) =>{
            console.log(response.status);
            if (response.status === 201){
                console.log(response);
                setUser({...user,
                    name:'',
                    email: '',
                    password: '',
                    password_confirmation: ''
                });
                e.target.reset();
                setUserSession(response.data.data.token,response.data.data.user);
                navigate('/');
            }
    }).catch((err) =>{
            console.log(err);
            setError(err.response.data.message);
            if (err.response.status === 401 || err.response.status === 400 || err.response.status === 422){
                setError(err.response.data.message);
            }
        })
    }
    return (
        <>
            {error ? (
                <Alert variant="danger" onClose={() => setError(null)} dismissible>
                    <p>Oh snap! {error}</p>
                </Alert>
            ): ('')}
            <Card className='h-100 m-auto' style={{ width: '30rem' }}>
                <Card.Body>
                    <Form onSubmit={formHandaler} >
                        <Form.Group className="mb-3" controlId="formBasicnName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control name='name' onChange={inpurHandaler} type="text" placeholder="Enter name" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control name='email' onChange={inpurHandaler} type="email" placeholder="Enter email"/>
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control name='password' onChange={inpurHandaler} type="password" placeholder="Password"/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control name='password_confirmation' onChange={inpurHandaler} type="password" placeholder="Confirm password"/>
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Registration
                        </Button>
                    </Form>
                </Card.Body>
            </Card>
        </>
    );
}

export default Registration;

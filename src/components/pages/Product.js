import React from 'react'
import {Card, Button,Col,Row} from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
const Product = () => {
    const product = useSelector((state) => state.allProducts.products);
    // const allpro = product.data;
    console.log(product);
    const renderProducts = product?.map((pro,i) => {
        const {id,title,image,price} = pro;
        return(
            <Col key={i}>
                <Card className='h-100' style={{ width: '18rem' }} >
                    <Link to={`products/${id}`}>
                        <Card.Img variant="top" src={image} />
                    </Link>
                    <Card.Body>
                        <Card.Title>{title}</Card.Title>
                    </Card.Body>
                    <Card.Footer>
                        <Row>
                            <Col>
                                <Card.Subtitle>Price: {price}</Card.Subtitle>
                            </Col>
                            <Col>
                                <Button variant="primary">Add to cart</Button>
                            </Col>
                        </Row>
                    </Card.Footer>
                </Card>
            </Col>
        )
    });
  return (
    renderProducts
  )
}

export default Product
